const pesel = document.querySelector("#pesel");
const anwser = document.querySelector("#anwser");
const bday = document.querySelector("#bday");
const sex = document.querySelector("#sex");
const submit = document.querySelectorAll(".submit");
const form = document.querySelectorAll(".form");
const formOpt = document.querySelectorAll(".form-option");
const Check = document.querySelector(".check");


// sprawdzenie poprawności peselu i uzupełnianie danych:
function check() {
    if (pesel.value.length == 11) {
        const peselVal = pesel.value;
        console.log(peselVal);
        // pobieranie liczb
        let num1 = Number(peselVal.charAt(0));
        let num2 = Number(peselVal.charAt(1));
        let num3 = Number(peselVal.charAt(2));
        let num4 = Number(peselVal.charAt(3));
        let num5 = Number(peselVal.charAt(4));
        let num6 = Number(peselVal.charAt(5));
        let num7 = Number(peselVal.charAt(6));
        let num8 = Number(peselVal.charAt(7));
        let num9 = Number(peselVal.charAt(8));
        let num10 = Number(peselVal.charAt(9));
        let num11 = Number(peselVal.charAt(10));
        // obliczanie poprwności peeselu
        let score = String(1 * num1 + 3 * num2 + 7 * num3 + 9 * num4 + 1 * num5 + 3 * num6 + 7 * num7 + 9 * num8 + 1 * num9 + 3 * num10);
        score = score.charAt(2);

        if (10 - score == num11) {
            anwser.style.color = "#00ee00";
            anwser.innerHTML = "Pesel jest poprawny";

            // uzupełnianie danych:
            // data ur:
            const rr = String(num1) + String(num2);
            let mm = String(num3) + String(num4);
            const dd = String(num5) + String(num6);

            if (Number(dd) <= 31 || Number(dd) >= 1) {
                if (mm >= 21 && mm <= 32) {
                    mm = String(Number(mm - 20));
                    if (mm.length == 1) {
                        bday.value = dd + ".0" + mm + "." + "20" + rr;
                        // płeć:
                        if (Number(peselVal.charAt(9)) == 1 || Number(peselVal.charAt(9)) == 3 || Number(peselVal.charAt(9)) == 5 || Number(peselVal.charAt(9)) == 7 || Number(peselVal.charAt(9)) == 9) {
                            sex.value = "Mężczyzna";
                            submit[0].removeAttribute("disabled");
                        } else if (Number(peselVal.charAt(9)) == 0 || Number(peselVal.charAt(9)) == 2 || Number(peselVal.charAt(9)) == 4 || Number(peselVal.charAt(9)) == 6 || Number(peselVal.charAt(9)) == 8) {
                            sex.value = "Kobieta";
                            submit[0].removeAttribute("disabled");
                        } else {
                            sex.value = "Nie poprawny pesel";
                        }
                    } else {
                        bday.value = dd + "." + mm + "." + "20" + rr;
                        // płeć:
                        if (Number(peselVal.charAt(9)) == 1 || Number(peselVal.charAt(9)) == 3 || Number(peselVal.charAt(9)) == 5 || Number(peselVal.charAt(9)) == 7 || Number(peselVal.charAt(9)) == 9) {
                            sex.value = "Mężczyzna";
                            submit[0].removeAttribute("disabled");
                        } else if (Number(peselVal.charAt(9)) == 0 || Number(peselVal.charAt(9)) == 2 || Number(peselVal.charAt(9)) == 4 || Number(peselVal.charAt(9)) == 6 || Number(peselVal.charAt(9)) == 8) {
                            sex.value = "Kobieta";
                            submit[0].removeAttribute("disabled");
                        } else {
                            sex.value = "Nie poprawny pesel";
                        }
                    }
                } else if (mm >= 1 && mm <= 12) {
                    bday.value = dd + "." + mm + "." + "19" + rr;
                    // płeć:
                    if (Number(peselVal.charAt(9)) == 1 || Number(peselVal.charAt(9)) == 3 || Number(peselVal.charAt(9)) == 5 || Number(peselVal.charAt(9)) == 7 || Number(peselVal.charAt(9)) == 9) {
                        sex.value = "Mężczyzna";
                        submit[0].removeAttribute("disabled");
                    } else if (Number(peselVal.charAt(9)) == 0 || Number(peselVal.charAt(9)) == 2 || Number(peselVal.charAt(9)) == 4 || Number(peselVal.charAt(9)) == 6 || Number(peselVal.charAt(9)) == 8) {
                        sex.value = "Kobieta";
                        submit[0].removeAttribute("disabled");
                    } else {
                        sex.value = "Nie poprawny pesel";
                    }
                } else if (mm >= 41 && mm <= 52) {
                    mm = String(Number(mm - 40));
                    
                    if (mm.length == 1) {
                        bday.value = dd + ".0" + mm + "." + "21" + rr;
                        // płeć:
                        if (Number(peselVal.charAt(9)) == 1 || Number(peselVal.charAt(9)) == 3 || Number(peselVal.charAt(9)) == 5 || Number(peselVal.charAt(9)) == 7 || Number(peselVal.charAt(9)) == 9) {
                            sex.value = "Mężczyzna";
                            submit[0].removeAttribute("disabled");
                        } else if (Number(peselVal.charAt(9)) == 0 || Number(peselVal.charAt(9)) == 2 || Number(peselVal.charAt(9)) == 4 || Number(peselVal.charAt(9)) == 6 || Number(peselVal.charAt(9)) == 8) {
                            sex.value = "Kobieta";
                            submit[0].removeAttribute("disabled");
                        } else {
                            sex.value = "Nie poprawny pesel";
                        }
                    } else {
                        bday.value = dd + "." + mm + "." + "21" + rr;
                        // płeć:
                        if (Number(peselVal.charAt(9)) == 1 || Number(peselVal.charAt(9)) == 3 || Number(peselVal.charAt(9)) == 5 || Number(peselVal.charAt(9)) == 7 || Number(peselVal.charAt(9)) == 9) {
                            sex.value = "Mężczyzna";
                            submit[0].removeAttribute("disabled");
                        } else if (Number(peselVal.charAt(9)) == 0 || Number(peselVal.charAt(9)) == 2 || Number(peselVal.charAt(9)) == 4 || Number(peselVal.charAt(9)) == 6 || Number(peselVal.charAt(9)) == 8) {
                            sex.value = "Kobieta";
                            submit[0].removeAttribute("disabled");
                        } else {
                            sex.value = "Nie poprawny pesel";
                        }
                    }
                } else {
                    bday.value = "Nie poprawny pesel";
                }
            } else {
                anwser.style.color = "#ee0000";
                anwser.value = "Pesel jest nie poprawny";
            }

        } else {
            anwser.style.color = "#ee0000";
            anwser.innerHTML = "Pesel jest nie poprawny";
            bday.value = "(dd/mm/rrrr)";
            sex.value = "(mężczyzna/kobieta)";
        }
    } else if (pesel.value.length > 11) {
        anwser.innerHTML = "Podałeś za długi pesel";
        anwser.style.color = "#fcba03";
    } else {
        anwser.innerHTML = "Wprowadź pełny pesel";
        anwser.style.color = "#ebeb28";
    }
}